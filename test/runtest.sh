#!/bin/bash
# Run QEMU for esp8266 with image.
# http://wiki.linux-xtensa.org/index.php/Xtensa_on_QEMU
# --------------------------------------------------------------

IMAGE="$(pwd)/../PiBox/imlightsw"

# qemu-system-xtensa -cpu dc232b -M sim -m 128M -semihosting -nographic -monitor null \
#     -kernel sim-2.6.29-smp/arch/xtensa/boot/Image.elf

qemu-system-xtensa -machine esp8266 -M sim -m 128M -semihosting -nographic -monitor null \
    -kernel sim-2.6.29-smp/arch/xtensa/boot/Image.elf
