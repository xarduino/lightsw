# Build for Arduino-based, PiBox controlled
# IoT Device based on ESP8266.
#
# ----------------------------------------------------------------------
# Directory and other setup
# ----------------------------------------------------------------------
# Top of ESP8266 related utilites (Arduino, makeEspArduino, etc.)
# ESPTOP should be set in your copy of docs/arduino.sh
ESP_TOP = $(ESPTOP)

# Esp8266 Arduino git location
# If this is a symlink, make sure to place a "/" at the end!
ESP_ROOT = $(ESP_TOP)/arduino/

# makeEspArduino git location
ESP_MK_TOP = $(ESP_TOP)/makeEspArduino
ESP_MK     = $(ESP_MK_TOP)/makeEspArduino.mk

# Where are the ESP libraries stored?
ESPLIBDIR = $(ESP_ROOT)/libraries

# Output directory
BUILD_ROOT = $(GM_BUILD)
BUILD_DIR  = $(BUILD_ROOT)

# makeEspArduino Overrides
# See top of $(ESP_MK) for what you can configure.

# ----------------------------------
# Configurable upload parameters
# UPLOAD_SPEED ?= 230400
# UPLOAD_PORT ?= /dev/ttyUSB0
# UPLOAD_VERB ?= -v
# ----------------------------------
UPLOAD_SPEED = 115200
UPLOAD_PORT  = $(shell echo $(SERIALDEV))
# UPLOAD_VERB  = -v

# ----------------------------------
# Configurable board parameters
# See "make list_flash_defs"
FLASH_DEF = 1M64

# ----------------------------------------------------------------------
#  Project setup
# ----------------------------------------------------------------------
# Which libraries are we using in our project?
# These work if they have .cpp files.  See also USER_DIRS.
# LIBS+=$(ESPLIBDIR)/Wire
# LIBS+=$(ESPLIBDIR)/EEPROM
# LIBS+=$(ESPLIBDIR)/ESP8266mDNS

# From Arduino Core
LIBS =$(ESPLIBDIR)/ESP8266WiFi
LIBS+=$(ESPLIBDIR)/ESP8266WebServer
LIBS+=$(ESPLIBDIR)/ESP8266HTTPClient
LIBS+=$(ESPLIBDIR)/WiFiManager
LIBS+=$(ESPLIBDIR)/DNSServer

# Required Downloads
LIBS+=$(ESPLIBDIR)/Timer
LIBS+=$(ESPLIBDIR)/AES
LIBS+=$(ESPLIBDIR)/aJson
LIBS+=$(ESPLIBDIR)/arduino-base64

# Name of our "sketch" (a .ino, aka .c, file)
SKETCH = PiBox/imlightsw.ino

# ----------------------------------------------------------------------
# Now includes the makefile that makes building with GNU Make easier.
# ----------------------------------------------------------------------
include $(ESP_MK_TOP)/makeEspArduino.mk

# Set the default device type, overridden by command line.
IMTYPE = "light_switch"
IMDESC = "Toggle light switch"

# Add include directories like this.  
# INCLUDE_DIRS += $(SDK_ROOT)/lwip2/include

# This is required for libraries which provide .h files only - no .cpp files.
# USER_DIRS+=$(ESPLIBDIR)/aREST

# Updated CFLAGS
C_DEFINES = -DIMTYPE=\"$(IMTYPE)\" -DIMDESC=\"$(IMDESC)\"

ifneq ($(RESET),)
C_DEFINES += -DRESET
endif

# Use serial console instead of Blue LED
ifneq ($(SERIAL),)
C_DEFINES += -DUSE_SERIAL
endif

# Enable Demo Mode, which switches the Relay on and off at 1sec intervals.
ifneq ($(IOTDEMO),)
C_DEFINES += -DIOT_DEMO_MODE
endif

# Enabled debugging in the core
ifneq ($(DEBUGV),)
C_DEFINES += -DDEBUG_ESP_CORE -DDEBUG_ESP_PORT=Serial1 -DDEBUG_CORE
endif

BUILD_EXTRA_FLAGS=$(C_DEFINES)

# Use VERBOSE=1 to have verbose build
ifneq ($(VERBOSE),)
CPP += -v
endif

# ----------------------------------------------------------------------
# Project specific targets
# ----------------------------------------------------------------------
clobber:
	@echo "Removing $(BUILD_ROOT)"
	rm -rf $(BUILD_ROOT)

showconfig:
	echo "ESP_TOP     : $(ESP_TOP)"
	echo "ESP_ROOT    : $(ESP_ROOT)"
	echo "ESPLIBDIR   : $(ESPLIBDIR)"
	echo "SDK_ROOT    : $(SDK_ROOT)"
	echo "BUILD_ROOT  : $(BUILD_ROOT)"
	echo "INCLUDE_DIRS: $(INCLUDE_DIRS)"
	echo "UPLOAD_SPEED: $(UPLOAD_SPEED)"
	echo "UPLOAD_PORT : $(UPLOAD_PORT)"
	echo "FLASH_DEF   : $(FLASH_DEF)"
	echo "C_INCLUDES  : $(C_INCLUDES)"
	echo "USER_DIRS   : $(USER_DIRS)"

