## How to use this repository

### We assume you have

* Connected ESP8266 to your serial port using an FTDI board
  * Try the one from [SparkFun](https://www.sparkfun.com/products/9873)
* You have proper power to the ESP8266
  * You need 3.3V for the ESP8266.
  * You need 5V for the power relay.
  * Try this board from [Amazon](https://smile.amazon.com/gp/product/B010UJFVTU/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1). It can supply both 3.3V and 5V power on a half size breadboard like this one from [Adafruit](https://www.adafruit.com/product/64)
* Other parts you might want
  * ESP-01 [breadboard adapter](https://www.addicore.com/ProductDetails.asp?ProductCode=AD-BB-ADTR)
  * [Logic Level Converter](https://www.sparkfun.com/products/12009)
  * [5V 1-Channel Relay](https://www.ebay.com/itm/5V-10A-one-1-Channel-Relay-Module-With-optocoupler-For-PIC-AVR-DSP-ARM-Arduino/310636242802?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)
boards

## Hardware Setup

* Breadboard setup can be seen using Fritzing on the diagrams in the docs directory.
  * _imlightsw.fzz_ - Board setup for flashing an ESP-01.
  * _imlightsw-with-relay-2.fzz_ - Board setup for controlling the power relay.
  * Relay design is based on [ESP8266-01 Pin Magic](http://www.forward.com.au/pfod/ESP8266/GPIOpins/ESP8266_01_pin_magic.html)
* If you get garbeled output from the serial connection to the ESP8266 then try setting a different terminal type.  The ANSI terminal type seems to work better than VT102 with Minicom, for example.

## Software Setup

* Make a directory to hold required repo clones; call it "repos".
* Change to "repos"
* Clone the esp8266/Arduino repo: https://github.com/esp8266/Arduino.git
  * git checkout tags/2.1.0
* Clone the makeEspArduino repo: https://github.com/plerup/makeEspArduino.git
  * If ESP_ROOT points to symlink but doesn't use trailing slash: git checkout d13cf13602f2acc9be3baa3315911a4c037dc62d
  * Otherwise: git checkout tags/4.13.0
* Install libraries
  * cd Arduino/libraries
  * clone extra libraries (see Required Libraries below)
* If your USB/FTDI connector shows up as something other than /dev/USBFTDI then set SERIALDEV
    * export SERIALDEV=/dev/_your device_
* aJson has a [bug with flush()](https://github.com/interactive-matter/aJson/issues/89) that prevents compilation.
  Apply the patch found in the patch directory.

## Building From within this source tree

* Copy docs/arduino.sh to your cdtools directory (~/bin/env) and edit as appropriate.
  * Be sure to set ESPTOP
* Setup your environment for the build:
  *  . cdtools
  *  ard lightsw
  *  cd? (follow cloning instruction)
* Change to this repos directory: cdx
* make
  * Binary is in ../bld/*.bin
* make upload
  * "upload" target is in makeEspArduino.mk, not in local Makefile.
* make RESET=1
  * This causes stored data in the ESP-01 to be erased on power up.  After that, you need to reflash without this flag.
* make SERIAL=1
  * This causes the code to use the TX line for serial output for debugging purposes.  If this is not set then the blue LED is used instead and there is not serial console output.

Reboot the board, using a serial terminal to watch for debug messages.  On first boot you need to connect to the _imiot_ wifi access point - that's the ESP8266.  Go to 192.168.4.1 - that's the web server on the ESP8266.  Follow the on screen instructions for configuring the access point the ESP8266 will connect to.  Save and reboot.

Now go to your device - it's IP depends on how you configured your local network.  The IP address will be displayed on the serial console connected to the ESP8266.

## Required Arduino libraries
  * AES:            https://github.com/spaniakos/AES/
  * Timer:          https://github.com/JChristensen/Timer.git
  * WiFiManager:    https://github.com/tzapu/WiFiManager.git 
                    tag: 0.12
                    commit: 04d47882a7d662b46bb1b1dbecfe786e5bc9efa4
  * aJson:          https://github.com/interactive-matter/aJson.git
                    Requires patch in patch/ directory.
  * aREST:          https://github.com/marcoschwartz/aREST.git
  * arduino-base64: https://github.com/adamvr/arduino-base64
                    Requires patch in patch/ directory.

## Notes on libraries
* cd esp8266/libraries, then clone the extra libraries there
* AES library:  Remove the examples or the build will probably fail.

## Notes on Makefile

* See Makefile for where esp8266 clone directory should go (ESP_ROOT)
* See Makefile for where makeEspArduino clone directory should go (ESP_MK)
* See Makefile for where libraries should be cloned (ESPLIBDIR)
* makeEspArduino has a minor bug that prevents compiling the aJson library.  To fix it change
   USER_SRC = $(SKETCH) $(shell find $(LIBS) -name "*.cpp")
   to
   USER_SRC = $(SKETCH) $(shell find $(LIBS) -name "*.cpp" -o -name "*.c")

## References

* For details on the REST API used by this code, see the [REST API's](http://redmine.graphics-muse.org/projects/ironman/wiki/REST_APIs) page on the Ironman wiki.
* For details on the communications protocol used, see the [Communications Protocols](http://redmine.graphics-muse.org/projects/ironman/wiki/MVP_#Communications-Protocols) section of the MVP page on the Ironman Wiki.
* For details on the ESP8266 Arduino environment:
  * General information: https://github.com/esp8266/Arduino
  * Installation: https://arduino-esp8266.readthedocs.io/en/latest/installing.html
* [General Arduino tips](http://forum.arduino.cc/index.php?topic=97455.0)
* [esptool-ck](https://github.com/igrr/esptool-ck) - for manual flashing of binaries

## Notes

* The [Arduino IDE](https://www.arduino.cc/en/Main/Software) is NOT required to build this project (though it's supportling libraries are)!  Instead, use the makeEspArduino system, which wraps GNU Make around the Arduino libraries.


## Reflashing to official firmware
Note: This process is just in case you get lost and want to start over.

## Prequisites

* Get the firmware: https://www.espressif.com/en/products/hardware/esp8266ex/resources
  Use the non-OS version.  Unpack into a directory and look in the bin directory for files that
  are to be flashed.
* Get the flashing tool: https://github.com/espressif/esptool

Flash the files with this command:
> python esptool.py --port /dev/ttyUSB0 write_flash <addr> <file>

Where

* port      Is usually /dev/ttyUSB0 or perhaps /dev/ttyACM0 or similar
* addr      Is the address where you want to store the file
* file      Is the file to be uploaded

| Address | File |
| ------- | ---- |
| 0x7E000    | blank.bin |
| 0x3E000    | blank.bin |
| 0x00000    | boot_v1.7.bin |
| 0x7C000    | esp_init_data_default_v08.bin |
| 0x1000     | at/512+512/user1.1024.new.2.bin |

Note that the _user1_ file is in a subdirectory.

## Editing this file

Changes to this file can be tested online at [the GitHub markdown editor](https://jbt.github.io/markdown-editor/).

## License

0BSD
