/*******************************************************************************
 * PiBox IoT: ESP8266 controller
 *
 * imlightsw.ino:  Provide RESTful interface to control this device.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/

/* Core libraries */
#include <stdio.h>
#include <string.h>
#include <FS.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>

/* Third party libraries */
#include <AES.h>
#include <Timer.h>
#include <aJSON.h>
#include <Base64.h>

/* Version of the monitor we're compatible with. */
#define MONITOR_VERSION     "1.0"

/*
 * If defined, use the serial output instead of the Blue LED pin.
 */
#ifdef USE_SERIAL

#define PRINTLN(a)      Serial.println(a);
#define PRINT(a)        Serial.print(a);

/* Allows us to use the Rx pin as digital output, which we need for the relay. */
#define SERIAL_BEGIN(a) Serial.begin(a,SERIAL_8N1,SERIAL_TX_ONLY);

#define SETBLUELED(a)

#else   // USE_SERIAL

#define PRINTLN(a)
#define PRINT(a)
#define SERIAL_BEGIN(a)
#define SETBLUELED(a)   setBlueLed(a)

#endif

/*
 * Blue LED pin id.
 */
#define BLUE_LED_PIN    1

/*
 * Blink Patterns
 */
#define BP_CONNECTED            blinkPattern(4,500)
#define BP_CONFIG_MODE          blinkPattern(2,500)
#define BP_CONNECT_MODE         blinkPattern(5,125)
#define BP_PAIR1_MODE           blinkPattern(2,75)
#define BP_PAIR2_MODE           blinkPattern(1,200)
#define BP_NOT_PAIRED           blinkPattern(3,100)

/*
 * Filename for saving credentials.
 */
char *registration_path     = "/registration.txt";

/*
 * Config Mode AP SSID
 */
String apname = "imiot";

/*
 * In the case were setup fails, we don't want to do anything in our main loop.
 */
boolean resetMode = false;

/*
 * AP Config mode disabled by default.
 * If Config button is ON at boot, we go into AP config mode.
 * After config is complete, button must be set OFF and device power cycled.
 * If Config button is OFF at boot, but turned on after then we go into PAIR mode (LED solid).
 * After PAIRING (LED blinks) the button must be turned off for normal operation.
 */
boolean configMode = false;

/*
 * Multicast setup
 * This is the address/port we use to send a multicast message asking
 * for monitor's to respond.  The first response we get is the one we
 * try to pair with.
 */
#define MULTICAST_PORT          13911
IPAddress ipMulti( 234, 1, 42, 3 );

/*
 * Incrememented with each multicast message we send, to identify
 * async responses to them.
 */
static unsigned long multicastID = 0;

/*
 * Timer object functions are "scheduled" to run periodically.
 */
Timer t;
int webLoopID  = -1;        // Timer used for processing inbound web connections.
int registerID = -1;        // Timer used for Pairing with a Monitor.
int pairModeID = -1;        // Timer used to blink LEDs during Pair mode.

/*
 * Registration and encryption
 * The uuid is provided by the monitor as part of the pairing process.
 * The uuid identifies this device to the monitor and is used, in part, as the key.
 * The key is used to encrypt/decrypt messages to/from the monitor.
 */
String uuid           = "";
String mykey          = "";
String monitorAddress = "";

/*
 * Object used to handle encryption/decryption.
 */
AES myaes;

/* Initialization vector, which we'll randomize a bit */
byte ivb[N_BLOCK] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
uint64_t iv;

/*
 * Device type and version
 * These aren't really used yet but may be in the future as part of
 * the pairing process.
 */
#define MT_IRONMAN      7
#define MA_PAIR_IOT     1
static char msgType     = MT_IRONMAN;
static char msgAction   = MA_PAIR_IOT;

/*
 * This is the web server that accepts inbound messages.
 * HTTPS is too taxing on an ESP-01 so we encrypt the body of messages instead.
 */
ESP8266WebServer server(80);

/*
 * Handle auto-connection
 * Globally scoped so API can reset, if necessary.
 */
WiFiManager wifiManager;

/* 
 * NOTE: WiFi object is always available because we included ESP8266WiFi.h 
 * See https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/station-class.html
 */

/*
 * Strings: some must not be wrapped with F()!
 */
#define S_GETDEVICES        "getdevices"

/*
 * Shared variables
 */
String      body;
String      decryptedMessage;
char        *json_String;
aJsonObject *root;
aJsonObject *bodyJson;
aJsonObject *ivJson;
aJsonObject *msgJson;

/*
 * Operational Flags
 */
char flags = 0;
#define FL_EVENT_WEBLOOP        0x01   // If this flag is set then the webloop event is enabled.
#define FL_EVENT_REGISTER       0x02   // If this flag is set then the register event is enabled.
#define FL_REGISTERED           0x04   // If this flag is set then we're paired already.
#define FL_SPIFFS_OK            0x08   // If this flag is set then SPIFFS is available.

/*
 * Device state
 * Off: 0
 * On:  1
 */
int state = 0;

/*
 * Function Prototypes
 */
int     handleDeviceSET();
int     handleDeviceQUERY();
int     handleRegistration();
int     handleNotFound();
int     paired();
void    saveFile( char *path, String data );
String  readFile( char *path );
void    blinkPattern(int count, int interval);
void    relayOn(void);
void    relayOff(void);
void    generateIV(byte *ivb, int size);
int     isMonitorOk();
void    encodeMessage(String message);
void    decodeMessage(String message);

/*
 * ========================================================================
 * Device Specific Functions and variables
 * ========================================================================
 */

/*
 * ========================================================================
 * Name:   pairEnabled
 * Prototype:  int pairEnabled()( void )
 *
 * Description:
 * Checks GPIO pins to see if the pair button is enabled.
 *
 * Returns:
 * 1 if the the button is enabled or 0 (zero) if it is not.
 * ========================================================================
 */
int pairEnabled()
{
    boolean pairMode = false;

    /* Set pin modes */
    pinMode(0, OUTPUT);
    pinMode(2, INPUT);

    /* Set GPIO0 output low */
    // PRINTLN("PAIR Mode: Set GPIO0 low.");
    digitalWrite(0, LOW);

    /* check GPIO2 input to see if push button pressed connecting it to GPIO0 */
    // PRINTLN("PairMode: Checking GPIO2 state.");
    pairMode = (digitalRead(2) == LOW);

    /* Reset GPIO0 HIGH to turn off the relay */
    // PRINTLN("PairMode: Resetting GPIO0.");
    digitalWrite(0, HIGH);

    if ( pairMode )
        return(1);
    else
        return(0);
}

/*
 * ========================================================================
 * Name:   setupURIHandlers
 * Prototype:  void setupURIHandlers()( void )
 *
 * Description:
 * Sets the callbacks for specific URI patterns.
 * ========================================================================
 */
void setupURIHandlers()
{
    server.on("/set",       HTTP_POST, handleDeviceSET);
    server.on("/query",     HTTP_POST, handleDeviceQUERY);
    server.on("/register",  HTTP_GET,  handleRegistration);
    server.onNotFound(handleNotFound);
}

/*
 * ========================================================================
 * Name:   genConfig
 * Prototype:  void genConfig()( void )
 *
 * Description:
 * Generates an encrypted JSON packet with our current configuration.
 * ========================================================================
 */
void genConfig()
{
    String message;

    /* Build our JSON response */
    root = aJson.createObject();
    aJson.addStringToObject(root, "uuid", uuid.c_str());
    aJson.addStringToObject(root, "type", IMTYPE);
    aJson.addStringToObject(root, "description", IMDESC);
    aJson.addNumberToObject(root, "state", state);
    json_String = aJson.print(root);
    message = String(json_String);
    aJson.deleteItem(root);
    PRINT("Message: "); PRINTLN(message);
    encodeMessage(message);
}

/*
 * ========================================================================
 * Name:   handleDeviceSET
 * Prototype:  int handleDeviceSET()( void )
 *
 * Description:
 * A SET is used to change device state.  The message field of the JSON
 * in the body is an BASE64 encoded AES field containing JSON, with a
 * state field, where state is OFF or ON (as text strings).
 * ========================================================================
 */
int handleDeviceSET()
{
    String          message;
    aJsonObject    *stateJson;

    /*
     * Validate the request.
     */
    if ( ! paired() )
    {
        server.send(401, "text/plain", "Failed");
        return(1);
    }

    /*
     * Reject requests from someone other than our registered monitor.
     */
    if ( ! isMonitorOk() )
    {
        server.send(401, "text/plain", "Failed");
        return(1);
    }

    /*
     * Extract body of message.
     */
    if (server.hasArg("plain")== false)
    {
        server.send(400, "text/plain", "Body not received");
        return(1);
    }
    body = server.arg("plain");

    /* Decode and decrypt it */
    decodeMessage(body);

    /*
     * Parse the message.
     */
    bodyJson  = aJson.parse( (char *)decryptedMessage.c_str() );
    stateJson = aJson.getObjectItem(bodyJson, "state");
    if ( (stateJson == NULL) || (stateJson->type != aJson_String) )
    {
        PRINTLN("Missing state request.");
        server.send(401);
        return(1);
    }

    /* Extract the new state. */
    char *stateStr = stateJson->valuestring;
    PRINT("New Device state: "); PRINTLN(stateStr);

    /*
     * Process request.
     */
    if ( strcasecmp(stateStr, "Off") == 0 )
    {
        relayOff();
        state = 0;
    }
    else if ( strcasecmp(stateStr, "On") == 0 )
    {
        relayOn();
        state = 1;
    }
    else
    {
        // Deny the request.
        PRINT(F("Bad state change request: requested state = ")); PRINTLN(stateStr);
        aJson.deleteItem(bodyJson);
        server.send(400);
        return(1);
    }
    
    // Ack the request.
    aJson.deleteItem(bodyJson);
    genConfig();
    message = String(json_String);
    server.send(200, "text/plain", (char *)message.c_str());
}

/*
 * ========================================================================
 * Name:   handleDeviceQUERY
 * Prototype:  int handleDeviceQUERY()( void )
 *
 * Description:
 * A GET is used to retrieve device state.  The body of the message is AES
 * encoded in the following format:
 *    UUID+JSON
 * where state is OFF or ON (as text strings).
 *
 * Notes:
 * See https://github.com/spaniakos/AES/blob/master/examples_Rpi/aes.cpp
 * For random number of iv: https://www.tutorialspoint.com/arduino/arduino_random_numbers.htm
 * Random seed should come from TX or RX in operational mode, but won't matter otherwise.
 * ========================================================================
 */
int handleDeviceQUERY()
{
    String          message;
    String          ptr;

    PRINTLN(F("Entered handleDeviceQUERY."));

    /*
     * Reject requests when we're not paired.
     */
    if ( ! paired() )
    {
        PRINTLN(F("Not paired."));
        server.send(401, "text/plain", "Failed");
        return(1);
    }

    /*
     * Reject requests from someone other than our registered monitor.
     */
    if ( ! isMonitorOk() )
    {
        PRINTLN(F("Invalid monitor."));
        server.send(401);
        return(1);
    }

    /*
     * Extract body of message.
     */
    if (server.hasArg("plain")== false)
    {
        server.send(400, "text/plain", "Body not received");
        return(1);
    }
    body = server.arg("plain");

    /* Decode request, to make sure requestor is really our paired monitor. */
    decodeMessage(body);
    if ( strncmp(decryptedMessage.c_str(), S_GETDEVICES, strlen(S_GETDEVICES)) != 0 )
    {
        PRINTLN(F("Invalid request - didn't find \"getdevices\"."));
        server.send(401);
        return(1);
    }

    /* Build our JSON response */
    genConfig();
    message = String(json_String);

    /* Send response */
    server.send(200, "text/plain", (char *)message.c_str());
}

/*
 * ========================================================================
 * Name:   handleRegistration
 * Prototype:  int handleRegistration()( void )
 *
 * Description:
 * A UUID is passed from the monitor in response to a multicast 
 * registration request.  This is the server's way of saying
 * "You are UUID".
 *
 * Since this is our first contact we have to pass this in the open.
 * Because of that we make it simple and just pass the UUID in
 * the URI, as such:  /register?uuid=<value>.
 * 
 * If the server responds with HTTP 200 then we're registered.
 *
 * Notes:
 * Insecure?  Sure.  But it requires someone listening precisely for two
 * specific, non-repeated messages to know exactly how to hack this device.
 * The uuid is never passed in clear text on-the-wire again.
 *
 * And yes, I thought about HTTPS, but ESP8266's (specifically ESP-01's)
 * really can't do those.  AES encrypted and encoded JSON is good enough.
 *
 * Imporant:
 * Once registered if you move your monitor (re: server) IP then you
 * have to re-pair!
 * ========================================================================
 */
int handleRegistration()
{
    HTTPClient      http;
    String          message = "";
    String          path = server.uri();

    /*
     * If pairing is not enabled, reject this request.
     */
    if ( !(flags & FL_EVENT_REGISTER) )
    {
        PRINTLN("Not in registration mode.");
        server.send(403);
        return(0);
    }

    /*
     * Do we already have a UUID?  If so, ignore this request.
     * But don't tell the requester why.  They should know already.
     */
    if ( flags & FL_REGISTERED )
    {
        message = "UUID already set; registration request will be quietly ignored.";
        PRINTLN(message);
        server.send(200, "text/plain", "Okay");
        return(0);
    }

    /* Clear any previous UUID */
    uuid = "";

    /* Grab the uuid from the URL */
    for (uint8_t i=0; i<server.args(); i++)
    {
        if ( strcasecmp(server.argName(i).c_str(), "uuid") == 0 )
        {
            uuid = String(server.arg(i));
            break;
        }
    }

    if ( strcmp(uuid.c_str(),"") == 0 )
    {
        message = "Missing required UUID component: uri = ";
        message += server.uri();
        PRINTLN(message);
        server.send(400, "text/plain", "Failed");
        return(1);
    }

    if ( uuid.length() < 16 )
    {
        message = "UUID length is too short (<16 characters)";
        PRINTLN(message);
        server.send(406, "text/plain", "Failed");
        return(1);
    }

    /* Gen AES key from UUID (like Jarvis) */
    mykey = uuid.substring(0,16);

    /* Save the monitor's IP address */
    monitorAddress = server.client().remoteIP().toString();

    /* Ack the request. */
    server.send(202, "text/plain", "Ok");

    /* 
     * Now complete the registration by calling the monitor's registration REST API: POST /pair/iot/register/UUID
     * Use ESP8266HTTPClient to contact the monitor:
     *      See https://techtutorialsx.com/2016/07/21/esp8266-post-requests/
     */

    String url = "http://" + monitorAddress + ":8165/pair/iot/" + uuid;
    PRINTLN("Registering with url = " + url);
    http.begin(url);
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Accept-Version", MONITOR_VERSION);
    http.addHeader("User-Agent", "miot");

    /*
     * Pairing includes a device type and description.
     * We don't need to add the UUID here because it's in the URL in this case.
     */
    root = aJson.createObject();
    aJson.addStringToObject(root, "type", IMTYPE);
    aJson.addStringToObject(root, "description", IMDESC);
    aJson.addNumberToObject(root, "state", state);
    json_String = aJson.print(root);
    message = String(json_String);
    aJson.deleteItem(root);

    int httpCode = http.POST(message);
    PRINT(F("Return code from http.POST: "));
    PRINTLN(httpCode);
    if ( httpCode != 200 )
    {
        message = "Failed registration: code = " + httpCode;
        PRINTLN(message);
    }
    else
    {
        PRINTLN(F("Registration OK."));

        /* Reformat SPIFFS - it may not have ever been formatted. */
        if ( !SPIFFS.exists(registration_path) && !SPIFFS.format() )
        {
            PRINTLN(F("Failed to format SPIFFS."));
        }
        else
        {
            PRINTLN(F("Formatted SPIFFS."));
        }

        /* Save registration data. */
        String registration_info = String(uuid + ":" + mykey + ":" + monitorAddress + "\n");
        saveFile(registration_path, registration_info);

        /* Validate */
        registration_info = readFile(registration_path);
        PRINT(F("Validating registration data: ")); PRINTLN(registration_info);

        /* Mark us a registered */
        flags |= FL_REGISTERED;
    }
}

/*
 * ========================================================================
 * Name:   pairPing
 * Prototype:  int pairPing( void )
 *
 * Description:
 * Ping the configured monitor to make sure we're still registered.
 *
 * Returns:
 * 1 if we're still registered.
 * 0 if we're not registered (re: the monitor is not authorizing us).
 * ========================================================================
 */
int pairPing()
{
    HTTPClient      http;
    String          message;

    /* Encode a ping message. */
    encodeMessage(String(F("ping")));
    message = String(json_String);
    PRINT(F("Ping JSON: ")); PRINTLN(message);

    String url = "http://" + monitorAddress + ":8165/ping";
    PRINTLN("Checking registering with url = " + url);
    http.begin(url);
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Accept-Version", MONITOR_VERSION);
    http.addHeader("User-Agent", "miot");

    int httpCode = http.POST(message);
    aJson.deleteItem(root);

    PRINT(F("Return code from http.POST: ")); PRINTLN(httpCode);
    if ( httpCode != 200 )
    {
        return(0);
    }
    else
    {
        return(1);
    }
}

/*
 * ========================================================================
 * Support Functions
 * ========================================================================
 */

/*
 * ========================================================================
 * Name:   encodeMessage
 * Prototype:  void encodeMessage( String message )
 *
 * Description:
 * Encrypt and encode the provided message and store in a JSON object.
 *
 * Returns:
 * A String object formated as a JSON message with "iv" and "message" fields.
 * ========================================================================
 */
void encodeMessage(String message)
{
    PRINT(F("Encoding message: "));PRINTLN(message);

    /* Get an IV */
    generateIV((byte *)&iv, sizeof(uint64_t));
    myaes.set_IV(iv);
    myaes.get_IV(ivb);

    /* Byte64 encode IVL as a string */
    int encodedLen = base64_enc_len(N_BLOCK);
    char ivb_b64[encodedLen];
    base64_encode((char *)ivb_b64, (char *)ivb, N_BLOCK);

    /* Need properly sized buffers for output from AES encryption */
    int plainPaddedLength = message.length() + (N_BLOCK - ((message.length()-1) % 16));
    byte cipher[plainPaddedLength];

    /* Encrypt the message. */
    myaes.do_aes_encrypt((byte *)message.c_str(), (int)message.length(), cipher, (byte *)mykey.c_str(), 128, ivb);

    myaes.set_IV(iv);
    myaes.get_IV(ivb);

    /* Debug: Decrypt the message. */
    byte new_message[plainPaddedLength];
    myaes.do_aes_decrypt((byte *)cipher, myaes.get_size(), (byte *)new_message, (byte *)mykey.c_str(), 128, ivb);
    if ( memcmp((char *)new_message, (char *)message.c_str(), message.length()) != 0 )
    {
        PRINTLN("Decrypted != Encrypted");
    }

    /* Byte64 encode encrypted message as a string */
    encodedLen = base64_enc_len(myaes.get_size());
    char message_b64[encodedLen];
    base64_encode((char *)message_b64, (char *)cipher, myaes.get_size());

    PRINT(F("iv: ")); PRINTLN(ivb_b64);
    PRINT(F("message: ")); PRINTLN(message_b64);

    /* Build the JSON response packet.  This is the encrypted version of our message. */
    root = aJson.createObject();
    aJson.addStringToObject(root, "iv", ivb_b64);
    aJson.addStringToObject(root, "message", message_b64);
    json_String=aJson.print(root);
}

/*
 * ========================================================================
 * Name:   decodeMessage
 * Prototype:  void decodeMessage( String message )
 *
 * Description:
 * Decode and decrypt the provided message and return as a string.
 *
 * Returns:
 * A String object whose format is context specific.
 * ========================================================================
 */
void decodeMessage(String message)
{
    decryptedMessage = String("");

    PRINT(F("Decoding message: "));PRINTLN(message);

    /* Import string as JSON */
    bodyJson = aJson.parse( (char *)message.c_str() );
    ivJson   = aJson.getObjectItem(bodyJson, "iv");
    msgJson  = aJson.getObjectItem(bodyJson, "message");

    if ( (ivJson == NULL) || (ivJson->type != aJson_String) )
    {
        PRINTLN(F("Missing IV in JSON to decode."));
        return;
    }
    if ( (msgJson == NULL) || (msgJson->type != aJson_String) )
    {
        PRINTLN(F("Missing message in JSON to decode."));
        return;
    }

    /* Decode IV: If it's not big enough, it's invalid anyway. */
    PRINT(F("iv: *")); PRINT(ivJson->valuestring); PRINTLN(F("*"));
    PRINT(F("IV len: ")); PRINTLN(strlen(ivJson->valuestring));
    int ivLen=base64_decode((char *)ivb, (char *)ivJson->valuestring, strlen(ivJson->valuestring));
    if ( ivLen != N_BLOCK )
    {
        PRINTLN(F("Decoded iv is != N_BLOCK"));
    }

    /* Decode Message */
    PRINT(F("message: ")); PRINTLN(msgJson->valuestring);
    PRINT(F("message len: ")); PRINTLN(strlen(msgJson->valuestring));
    int decodedLen = base64_dec_len(msgJson->valuestring, strlen(msgJson->valuestring));
    PRINT(F("1. decode message len: ")); PRINTLN(decodedLen);
    char message_b64[decodedLen];
    decodedLen=base64_decode((char *)message_b64, (char *)msgJson->valuestring, strlen(msgJson->valuestring));
    PRINT(F("2. decode message len: ")); PRINTLN(decodedLen);

    /* Decrypt message using IV */
    int paddedLength = decodedLen + N_BLOCK - decodedLen % N_BLOCK;
    PRINT(F("paddedlen: ")); PRINTLN(paddedLength);
    char decryptBuf[paddedLength];
    memset(decryptBuf, 0, paddedLength);
    PRINT(F("key: ")); PRINTLN(mykey.c_str());
    myaes.do_aes_decrypt((byte *)message_b64, decodedLen, (byte *)decryptBuf, (byte *)mykey.c_str(), 128, (byte *)ivb);

    /*
     * We might get extra characters in our buffer since it didn't know exactly how many characters to process.
     * Therefore, first non-printable character must be replaced with termination character.
     */
    char *ptr = decryptBuf;
    int i;
    for(i=0; i<paddedLength; i++)
    {
        if (!isprint((int)*ptr) ) 
        {
            *ptr = '\0';
            break;
        }
        ptr++;
    }

    /* Return string */
    decryptedMessage = String(decryptBuf);
    PRINT(F("Decrypted message: ")); PRINTLN(decryptBuf);
}

/*
 * ========================================================================
 * Name:   generate_random_uint8
 * Prototype:  uint8_t generate_random_uint8( )
 *
 * Description:
 * Generate a random unsigned 8 bit value.
 * ========================================================================
 */
uint8_t generate_random_uint8()
{
    uint8_t really_random = *(volatile uint8_t *)0x3FF20E44;
    return really_random;
}

/*
 * ========================================================================
 * Name:   generateIV
 * Prototype:  void generateIV(byte *ivdata, int size)
 *
 * Description:
 * Generate a series of random 8 bit values sufficient for use as an
 * initialization vector.
 * ========================================================================
 */
void generateIV(byte *ivdata, int size)
{
    byte *ptr = ivdata;
    for (int i = 0; i < size; i++)
    {
        *ptr = (byte)generate_random_uint8();
        ptr++;
    }
}

/*
 * ========================================================================
 * Name:   isMonitorOk
 * Prototype:  int isMonitorOk()()
 *
 * Description:
 * Verify a request comes from the registered monitor.
 *
 * Returns:
 * 0 if the request is not from the registered monitor.
 * 1 if the request is from the registered monitor.
 * ========================================================================
 */
int isMonitorOk()
{
    String requestAddress = server.client().remoteIP().toString();
    PRINT("Monitor Address: "); PRINTLN(monitorAddress);
    PRINT("Request Address: "); PRINTLN(requestAddress);
    if ( strcmp(requestAddress.c_str(), monitorAddress.c_str()) == 0 )
    {
        return(1);
    }
    else
    {
        return(0);
    }
}

/*
 * ========================================================================
 * Name:   paired
 * Prototype:  int paired()( void )
 *
 * Description:
 * Check if this device is already paired.
 *
 * Returns:
 * 0 if we are not paired or the request comes from the wrong monitor IP.
 * 1 if we are paired and the request comes from the saved monitor IP.
 * ========================================================================
 */
int paired()
{
    /*
     * Are we registered?  That's the basic requirement.
     */
    if ( !(flags & FL_REGISTERED) )
    {
        PRINTLN(F("FL_REGISTERED is not set."));
        return(0);
    }

    /* Looks good. */
    return(1);
}

/*
 * ========================================================================
 * Name:   webLoop
 * Prototype:  void webLoop( void )
 *
 * Description:
 * Check the web server for client connections.
 * ========================================================================
 */
void webLoop()
{
    // PRINTLN("Testing for web connections.");
    PRINT(".");
    server.handleClient();
}

/*
 * ========================================================================
 * Name:   pairModeBlink
 * Prototype:  void pairModeBlink( void )
 *
 * Description:
 * Display blink pattern when in PairMode.
 * ========================================================================
 */
void pairModeBlink()
{
    if ( flags & FL_EVENT_REGISTER )
    {
        BP_PAIR1_MODE;
        BP_PAIR2_MODE;
    }
}

/*
 * ========================================================================
 * Name:   notRegisteredBlink
 * Prototype:  void notRegisteredBlink( void )
 *
 * Description:
 * Display blink pattern when not registered
 * ========================================================================
 */
void notPairedBlink()
{
#ifdef USE_SERIAL
    PRINTLN("Not Paired - need to pair with a monitor.");
#else
    BP_NOT_PAIRED;
#endif
}

/*
 * ========================================================================
 * Name:   deviceRegister
 * Prototype:  void deviceRegister()( void )
 *
 * Description:
 * Send registration request via multicast until registration completes.
 *
 * Notes:
 * Multicast request is Ack'd by server.  We use that Ack to identify
 * the server IP so we can contact it via http to complete registration.
 *
 * The data attached to the multicast message is actually not used yet.
 * ========================================================================
 */
void deviceRegister()
{
    String message;

    /*
     * Don't run if we're not actually in PAIR mode.
     */
    if ( !(flags & FL_EVENT_REGISTER) )
    {
        return;
    }

    /* Skip if Wifi is not running. */
    if (WiFi.status() != WL_CONNECTED)
        return;

    /*
     * Send multicast request.  The outbound message has the following format.
     * Byte 0:      Message type  (MT_IRONMAN)
     * Byte 1:      Action type (MA_PAIR_IOT)
     * Byte 3-4:    Unused
     * Byte 5-8:    Integer size of payload (0, but payload size is 4 bytes long)
     * If the server accepts those, it responds with an UUID via the web interface.
     */

    WiFiUDP Udp;
    Udp.beginPacketMulticast(ipMulti, MULTICAST_PORT, WiFi.localIP());
    Udp.write(msgType);
    Udp.write(msgAction);
    char buffer = 0;
    for(int i=0; i<6; i++)
    {
        Udp.write(buffer);
    }
    Udp.endPacket();

    PRINT(F("Sent multicast announcement: ")); PRINTLN(multicastID);
    multicastID++;
}

/*
 * ========================================================================
 * Command Handlers
 * ========================================================================
 */

/*
 * ========================================================================
 * Name:   reset
 * Prototype:  int reset( void )
 *
 * Description:
 * Reset WiFi connection, allowing reconfiguration.
 *
 * Notes:
 * Clears SPIFFS registration file too.
 * ========================================================================
 */
void reset()
{
    wifiManager.resetSettings();
    if ( SPIFFS.begin() )
    {
        if ( SPIFFS.exists(registration_path) && !SPIFFS.remove(registration_path) )
        {
            PRINT("Failed to remove old registration path: "); PRINTLN(registration_path);
        }
    }
    else
    {
        PRINTLN("Can't mount SPIFFS.");
    }
    flags          = 0;
    uuid           = "";
    mykey          = "";
    monitorAddress = "";
    delay(3000);
}

/*
 * ========================================================================
 * Name:   handleNotFound
 * Prototype:  int handleNotFound( void )
 *
 * Description:
 * Handle invalid requests.  This sends a text reply, which may not be
 * suitable for the remote caller.
 * ========================================================================
 */
int handleNotFound()
{
    String message = "";

    /*
     * Requests when we're not paired are rejected.
     */
    if ( ! paired() )
    {
        message  = "Not found and not paired: ";
        message += server.uri();
        PRINTLN(message);
        server.send(401, "text/plain", "Failed");
        return(1);
    }
    PRINTLN("Paired, but request is Not Found.");

    /*
     * Validate the request.  That's more important than telling them it's
     * a bad request.
     */
    message = "Invalid command\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++)
    {
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(400, "text/plain", message);
    PRINT("Sent: ");
    PRINTLN(message);
}

/*
 * ========================================================================
 * Name:   saveModeCallback
 * Prototype:  void saveModeCallback( void )
 *
 * Description:
 * Print status info if connection is lost.
 * ========================================================================
 */
void saveModeCallback()
{
    if (WiFi.status() != WL_CONNECTED)
    {
        PRINTLN("Failed connection to configured AP.");
        PRINT("AP: "); PRINTLN(WiFi.SSID());
        PRINT("PW: "); PRINTLN(WiFi.psk());
        PRINTLN("Trying again.");
    }
}

/*
 * ========================================================================
 * Pin Handlers
 * ========================================================================
 */

/*
 * ========================================================================
 * Name:   setBlueLed
 * Prototype:  void setBlueLed( boolean enabled )
 *
 * Description:
 * Set the state of the blue LED.  This only works if USE_SERIAL is
 * not defined.
 *
 * Notes:
 * LED is lit when the pin is set LOW.
 * ========================================================================
 */
void setBlueLed(boolean enabled)
{
    pinMode(BLUE_LED_PIN, OUTPUT);
    if ( enabled )
    {
        digitalWrite(1, LOW);
    }
    else
    {
        digitalWrite(1, HIGH);
    }
}

/*
 * ========================================================================
 * Name:   toggleRelay
 * Prototype:  void toggleRelay( void )
 *
 * Description:
 * Toggles the relay on and off. Used to validate relay circuit is working.
 * ========================================================================
 */
#ifdef IOT_DEMO_MODE
void toggleRelay(void)
{
    static int state = 0;
    if ( state == 0 )
    {
        relayOn();
        state = 1;
    }
    else
    {
        relayOff();
        state = 0;
    }
}
#endif

/*
 * ========================================================================
 * Name:   relayOn
 * Prototype:  void relayOn( void )
 *
 * Description:
 * Turns the relay on, enabling power to flow to the controlled device.
 * ========================================================================
 */
void relayOn(void)
{
    PRINTLN("Turning relay on.");
    digitalWrite(3, LOW);
}

/*
 * ========================================================================
 * Name:   relayOff
 * Prototype:  void relayOff( void )
 *
 * Description:
 * Turns the relay off, disabling power to the controlled device.
 * ========================================================================
 */
void relayOff(void)
{
    PRINTLN("Turning relay off.");
    digitalWrite(3, HIGH);
}

/*
 * ========================================================================
 * Name:   blinkPattern
 * Prototype:  void blinkPattern( int count, int interval )
 *
 * Description:
 * Blinks the BlueLED in a specified pattern.
 * ========================================================================
 */
void blinkPattern(int count, int interval)
{
    /* Blink patterns repeat "count" times, with "interval" milliseconds between patterns. */
    for(int i=0; i<count; i++)
    {
        SETBLUELED(true); delay(interval); SETBLUELED(false); delay(interval);
        PRINT(".");
    }
}

/*
 * ========================================================================
 * File Management
 * ========================================================================
 */

/*
 * ========================================================================
 * Name:   saveFile
 * Prototype:  void saveFile( char *path, String data )
 *
 * Description:
 * Saves data to the specified path.  The contents of the file associated 
 * with path will be replaced by the new data.
 * ========================================================================
 */
void saveFile( char *path, String data )
{
    String message;
    if ( ! (flags & FL_SPIFFS_OK) )
    {
        message = "SPIFFS not mounted.  Can't save to " + String(path);
        PRINTLN(message);
    }

    File f = SPIFFS.open(path, "w");
    if ( !f ) 
    {
        message = "Failed to open " + String(path) + " for writing.";
        PRINTLN(message);
    }
    else
    {
        f.println(data.c_str());
        f.flush();
        f.close();
        PRINT(F("Saved data: ")); PRINTLN(data);
        PRINT(F("To path   : ")); PRINTLN(String(path));
    } 

    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) 
    {
        f = dir.openFile("r");
        PRINT(F("File: ")); PRINT(dir.fileName());
        PRINT(F(", size: ")); PRINTLN(f.size());
    }

}

/*
 * ========================================================================
 * Name:   readFile
 * Prototype:  String readFile( char *path )
 *
 * Description:
 * Reads the file from the specified path and returns it as a single string,
 * including newlines.
 * ========================================================================
 */
String readFile( char *path )
{
    String message;
    if ( ! (flags & FL_SPIFFS_OK) )
    {
        message = "SPIFFS not mounted.  Can't read from " + String(path);
        PRINTLN(message);
    }

    String data = String("");
    File f = SPIFFS.open(path, "r");
    if ( f ) 
    {
        PRINTLN("Reading from SPIFFS");
        while(f.available()) 
        {
            //Lets read line by line from the file
            data.concat( f.readStringUntil('\n') );
            data.concat( String('\n') );
        }
        f.close();
        message = "Read from " + String(path) + ": " + data;
        PRINTLN(message);
    }
    else
    {
        message = "Failed to open " + String(path) + " for reading.";
        PRINTLN(message);
    }
    return(data);
}

/*
 * ========================================================================
 * Setup
 * ========================================================================
 */

/*
 * ========================================================================
 * Name:   setup
 * Prototype:  void setup( void )
 *
 * Description:
 * Initialization function; required by Arduino enviroment.
 * This function handles setup of WiFi connections and configures
 * URI handlers for the web server.
 * ========================================================================
 */
void setup(void)
{
    String  message;
    char    *pos;

    /*
     * Serial port output seems to work better at 9600.
     */
    SERIAL_BEGIN(115200);
    PRINTLN("");
    PRINTLN("Waiting on SDK.");
#ifdef USE_SERIAL
#ifdef DEBUG_CORE
    Serial.setDebugOutput(true);
#endif
#endif
    delay(3500);

    /*
     * Initial setup for using WifiManager.
     */
    wifiManager.setDebugOutput(true);
    wifiManager.setConnectTimeout(30);

    /*
     * Setup SPIFFS for saving data.
     */
    if ( !SPIFFS.begin() )
    {
        PRINTLN("Can't mount SPIFFS.");
    }
    else
    {
        PRINTLN("Mounted SPIFFS.");
        flags |= FL_SPIFFS_OK;
    }

#ifdef RESET
    /*
     * Use this to reset the board to it's default state.
     * Set RESET=1 on the GNU Make command line to use this.
     * Then after the board resets once, run the build without RESET=1
     * build/upload again.
     */
    PRINTLN("Resetting WiFiManager.");
    reset();
    PRINTLN("Done. Now reflash the board.");
    resetMode = true;
    return;

#endif // RESET

    /* Use Rx as a digital output to control the relay. */
    pinMode(3, OUTPUT);
    relayOff();

    /* Set GPIO0 output low, so we can check for Config mode. */
    PRINTLN("Set GPIO0 low.");
    pinMode(0, OUTPUT);
    digitalWrite(0, LOW);

    /*
     * Check GPIO2 input to see if push button is pressed (connecting it to GPIO0)
     * placing us in ConfigMode.
     */
    PRINTLN("Checking boot state.");
    configMode = (digitalRead(2) == LOW);
    if (configMode)
    {
        PRINTLN("In Config Mode.");
        BP_CONFIG_MODE;
        delay(1000);
        SETBLUELED(true);

        /*
         * Start AP and serve up web page configuration.
         */
        wifiManager.resetSettings();

        /*
         * Set callback to be notified when connection to newly configured
         * remote AP succeeds.
         */
        wifiManager.setSaveConfigCallback(saveModeCallback);

        while ( 1 )
        {
            wifiManager.startConfigPortal((char *)apname.c_str());
            if (WiFi.status() == WL_CONNECTED)
            {
                break;
            }
        }
        BP_CONNECTED;
        PRINTLN("");
        PRINT("Connected to "); PRINTLN(WiFi.SSID());
        PRINT("IP address: ");  PRINTLN(WiFi.localIP());

        /* 
         * Since we're resetting our connection we need to reset our registration.
         * There is no other way to reset registration (other than by compiling with
         * the RESET enabled) because we don't have enough GPIO pins on an ESP8266-01.
         */
        if ( SPIFFS.exists(registration_path) && !SPIFFS.remove(registration_path) )
        {
            PRINT("Failed to remove old registration path: "); PRINTLN(registration_path);
        }

        return;
    }
    else
    {
        /*
         * In operational mode we just try to connect to the last configured AP.
         * We don't use WiFiManager here because we don't want to drop back into the 
         * auto-configure AP on the device.  If connection fails with saved credentials
         * then we simply punt in this mode.
         */
        PRINTLN("In Operational Mode.");
        WiFi.begin();
        while ( 1 )
        {
            if (WiFi.status() == WL_CONNECTED)
            {
                break;
            }
            message = "Failed connection to ";
            message += WiFi.SSID();
            message += "; trying again.";
            PRINTLN(message);
            BP_CONNECT_MODE;
            delay(1500);
        }
        BP_CONNECTED;
        PRINTLN("imiot setup:");
        PRINT("Connected to: "); PRINTLN(WiFi.SSID());
        PRINT("IP address  : "); PRINTLN(WiFi.localIP());
        PRINT("Password    : "); PRINTLN(WiFi.psk());
    }

    /* Reset GPIO0 HIGH to make sure we're not in Config mode. */
    PRINTLN("Resetting GPIO0.");
    digitalWrite(0, HIGH);

    /*
     * Setup web server URI handlers: this is done in a function to make
     * it easy to find where device specific code will go.
     */
    setupURIHandlers();

    /* Notify user of status, if they're watching the serial console. */
    server.begin();
    PRINTLN(F("HTTP server started"));

    /* Load registration info, if any. */
    if ( SPIFFS.exists(registration_path) )
    {
        // split into: uuid, key and monitorAddress
        String registration = readFile(registration_path);
        char *reg_copy = strdup(registration.c_str());

        char *ptr = strtok(reg_copy, ":");
        uuid = String(strdup(ptr));
        PRINT(F("UUID: ")); PRINTLN(uuid);

        ptr = strtok(NULL, ":");
        mykey = String(strdup(ptr));
        PRINT(F("Key: ")); PRINTLN(mykey);

        ptr = strtok(NULL, ":");
        if ((pos=strchr(ptr, '\n')) != NULL)
            *pos = '\0';
        monitorAddress = String(strdup(ptr));
        PRINT(F("Monitor Address: ")); PRINTLN(monitorAddress);

        free(reg_copy);

        /* Verify we're still paired with the monitor. */
        if ( pairPing() )
        {
            PRINTLN(F("We're still paired."));

            /* Mark us a registered */
            flags |= FL_REGISTERED;
        }
        else
        {
            PRINTLN(F("We're not paired anymore.  Removing registration."));

            /* Notify the user that we're not registered. */
            SPIFFS.remove(registration_path);
            registerID = t.every(2000, notPairedBlink);
            resetMode = true;
            return;
        }
    }
    else
    {
        PRINTLN("Device registration not found.");
    }

    /* Setup scheduled events. */
    PRINTLN("Scheduling timer events.");
    registerID = t.every(5000, deviceRegister);
    webLoopID  = t.every(250, webLoop);
    pairModeID = t.every(10, pairModeBlink);

#ifdef IOT_DEMO_MODE
    t.every(1000, toggleRelay);
#endif

    /* Enable initial event handlers */
    flags |= FL_EVENT_WEBLOOP;
}

/*
 * ========================================================================
 * Name:   loop
 * Prototype:  void loop( void )
 *
 * Description:
 * Main loop function; required by Arduino enviroment.
 * This function uses the Timer API to spin, caling handlers at specified
 * intervals.
 * ========================================================================
 */
void loop(void)
{
    /* Do nothing here if we're in RESET mode. */
    if ( resetMode )
    {
        t.update();
        return;
    }

    /*
     * We don't do anything until user has reset from Config Mode.
     * That requires a power cycle.
     */
    if ( configMode )
    {
        BP_CONNECTED;
        delay(3000);
        return;
    }

    /*
     * If PAIR mode button is set, go into pairing mode.
     */
    if ( pairEnabled() )
    {
        /*
         * We should notify existing monitor that we're going away here.
         * TBD: requires new REST API.
         */

        /* Assume we're no longer registered. */
        flags &= ~FL_REGISTERED;
        flags |= FL_EVENT_REGISTER;

        do
        {
            t.update();
    
        } while ( !(flags & FL_REGISTERED) && pairEnabled() );

        /* Disable being in the register event */
        flags &= ~FL_EVENT_REGISTER;
    }
    else
    {
        /*
         * Operational Mode.
         */

        /* Check for timer events */
        t.update();
    }
}
