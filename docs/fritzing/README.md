This design uses add on parts.

Keyes Relay:  https://github.com/rwaldron/fritzing-components
Breadboard Power Supply: https://github.com/AchimPieters/Fritzing-Custom-Parts/blob/master/YwRobot%20Breadboard%20Power%20Supply.fzpz

Additional parts:
http://omnigatherum.ca/wp/?s=fritzing+parts
https://github.com/ydonnelly/ESP8266_fritzing
