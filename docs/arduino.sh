#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: Arduino projects
# Edit variables with <> values, as needed.
# -------------------------------------------------------------------
function ard {
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    SRCTOP=<PATH TO WHERE PROJECT DIRECTORY WILL LIVE>

    # ed.am arduino.mk configuration
    SERIALDEV=</dev/ttyUSB...>
    ARDUINODIR=<path to latest Arduino release>

    # ESP8266 specific (uses https://github.com/plerup/makeEspArduino.git)
    ESPTOP=<path to git clone of https://github.com/esp8266/Arduino.git

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # Project ID
    PARENT=xarduino

    # Project ID:  Defaults to "tmp102" unless first argument is set
    if [ "$1" != "" ]
    then
        case "$1" in
        "tmp102")       PRJ=$1;;
        "apc")          PRJ=$1;;
        "esp8266")      PRJ=$1;;
        "lightsw")      PRJ=$1;;
        *) 
            echo "Invalid repo"
            arduinorepos
            return 0
            ;;
        esac
    else
        PRJ=tmp102
    fi

    # GIT Repo
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git

    # Suffix allows for creating multiple trees for the same repo
    if [ "$1" != "" ]
    then
        SFX=$2
    else
        SFX=$1
    fi

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    mkdir -p $GM_WORK

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX

    # Where the source, build and package directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_PKG=$GM_HOME/pkg
    GM_ARCHIVE=$GM_HOME/archive
    GM_EXTRAS=$GM_HOME/extras

    # Make the configured environment available 
    export GM_SRC
    export GM_BUILD
    export GM_PKG
    export GM_ARCHIVE
    export GM_EXTRAS
    export GM_WORK
    export GM_HOME
    export SKETCHBOOK
    export ARDUINODIR
    export SERIALDEV
    export ESPTOP

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cde='cd $GM_EXTRAS'
    alias cdl='arduinorepos'

    # Show the aliases for this configuration
    alias cd?='listarduino'
}
function listarduino {
echo "
$PARENT $PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cdp    cd GM_PKG ($GM_PKG)
cde    cd GM_EXTRAS ($GM_EXTRAS)
cdl    List available repositories

Locations for ed.am arduino.mk (see http://ed.am/dev/make/arduino-mk):
SKETCHBOOK : $SKETCHBOOK
Serial Device: SERIALDEV = $SERIALDEV 
    try: 
        grep 'USB ACM device' /var/log/messages
        grep 'FTDI USB Serial Device converter' /var/log/messages
    You can also set SERIALDEV manually.
ESPTOP: $ESPTOP

To checkout tree:
cdt
mkdir $PRJ$SFX
cdh
git clone $GITREPO src

Pushing your local repository to Gitorious:
    cd <src>
    git init
    git add .
    git commit
    git checkout master
    git remote add origin $GITREPO
    git push origin master
"
}
function arduinorepos {
echo "
Arduino repos:
-----------------------------------------------------------------------------
tmp102:         Temperature Sensor (TI TMP102 w/2-wire Serial Interface)
apc:            Aeroponics power controller
esp8266:        ESP8266 experimental
lightsw:        Ironman Light Switch
"
}



